var fs = require('fs');
var path = require('path');
var homedir = process.env.HOME || process.env.USERPROFILE;
var sceHome=path.join(homedir, ".imsofa");
sceHome=path.join(sceHome, ".sce");
var appRegistryPath=path.join(sceHome, "apps.json");
var appRegistry={};
if(fs.existsSync(appRegistryPath)){
    appRegistry=JSON.parse(fs.readFileSync(appRegistryPath));
}
function resolveAppVersionString(appName){
    var versionArray=appRegistry[appName];
    if(versionArray){
        versionArray=versionArray.sort(function(a, b){
            if(a.majorVersion > b.majorVersion){
                return 1;
            }else if(a.majorVersion < b.majorVersion){
                return -1;
            }else if(a.minorVersion > b.minorVersion){
                return 1;
            }else if(a.minorVersion < b.minorVersion){
                return -1;
            }else if(a.buildNumber > b.buildNumber){
                return 1;
            }else if(a.buildNumber < b.buildNumber){
                return -1;
            }else{
                return 0;
            }
        });
        var latest=versionArray[versionArray.length-1];
        return latest.majorVersion+"."+latest.minorVersion+"."+latest.buildNumber;
    }else{
        return null;
    }
}

module.exports=resolveAppVersionString;