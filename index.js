#!/usr/bin/env node
var fs = require('fs');
var path = require('path');
var homedir = process.env.HOME || process.env.USERPROFILE;

var appName = "hello-1.0.0";
var command = "run";
if (process.argv.length >= 3) {
	if (process.argv[2] == "update") {
		command = "update";
	} else if(process.argv[2]=="run"){
		command="run";
		if(process.argv.length>=4){
			appName=process.argv[3];
		}
	}else {
		appName = process.argv[2];
	}
}


var folders = [".imsofa", ".sce", "apps"];
var current = homedir;
for (var i in folders) {
	var folder = folders[i];
	current = path.join(current, folder);
	if (!fs.existsSync(current)) {
		fs.mkdirSync(current);
	}
}
var sceHome=path.join(homedir, ".imsofa");
sceHome=path.join(sceHome, ".sce");

var appsFolder = path.join(homedir, ".imsofa", ".sce", "apps");
var appFolder = path.join(appsFolder, appName);
if (!fs.existsSync(appFolder)) {
	fs.mkdirSync(appFolder, { recursive: true });
}

if (command == "update") {
	const update=require('./command_update');
	update(sceHome);
} else {
	const run=require('./command_run');
	run(appFolder, appName);
}