var http = require('follow-redirects').https;
var fs = require('fs');
var unzip = require('extract-zip');
var spawn = require('child_process').spawn;
var path = require('path');
var resolveAppVersionString=require("./appVersionResolver.js");

module.exports=function(appFolder, appName){
    var index=appName.indexOf("-");
    if(index==-1){
        var versionString=resolveAppVersionString(appName);
        if(versionString){
            appName=appName+"-"+versionString;
            console.log("use app="+appName);
        }else{
            console.log("the requested app is not found!");
            return;
        }
    }
    var appArchive = appName + ".zip";
	var checkFile = path.join(appFolder, "package.json");
	var appArgs = [];

	for (var i = 3; i < process.argv.length; i++) {
		appArgs.push(process.argv[i]);
	}

	function executeApp() {
		var ls = spawn("npm", ['start'].concat(appArgs), {
			'cwd': appFolder
		});
		ls.stdout.on('data', function (data) {
			console.log(data.toString());
		});
	}

	function npmInstall() {
		var ls = spawn("npm", ['install'], {
			'cwd': appFolder
		});
		ls.stdout.on('data', function (data) {
			process.stdout.write(data.toString('utf-8'));
		});
		ls.on('close', function () {
			executeApp();
		});
	}

	if (fs.existsSync(checkFile) == false) {
		fs.mkdir(appFolder, {
			recursive: true
		}, function (err) {
			console.log("app folder created");
			var file = fs.createWriteStream(path.join(appFolder, appArchive));
			var request = http.get("https://github.com/lendle1028/PhysicalSingalAnalyzerCollectors/blob/master/" + appArchive + "?raw=true", function (response) {
				response.pipe(file);
				file.on('finish', function () {
					file.close();
					console.log("app archive downloaded");
					unzip(path.join(appFolder, appArchive), { dir: path.resolve(appFolder) }, function (err) {
						if (!err) {
							npmInstall();
						} else {
							console.log("fail to unzip");
						}
					});
					/*fs.createReadStream(path.join(appFolder, appArchive)).
					pipe(unzip.Extract({
						path: appFolder
					})).on('close', function () {
						npmInstall();
					});*/
				});
			});
		});
	} else {
		executeApp();
	}
};