var request = require('request');
var fs = require('fs');
var path = require('path');
module.exports=function(sceHome){
    request.get("https://api.github.com/repos/lendle1028/PhysicalSingalAnalyzerCollectors/commits", {
		headers: {
			'User-Agent': "chrome"
		}
	}, function (error, response, body) {
		var obj = JSON.parse(body);
		request.get("https://api.github.com/repos/lendle1028/PhysicalSingalAnalyzerCollectors/git/commits/" + obj[0].sha, {
			headers: {
				'User-Agent': "chrome"
			}
		}, function (error, response, body) {
			var obj = JSON.parse(body);
			request.get("https://api.github.com/repos/lendle1028/PhysicalSingalAnalyzerCollectors/git/trees/" + obj.tree.sha, {
				headers: {
					'User-Agent': "chrome"
				}
			}, function (error, response, body) {
                var obj=JSON.parse(body);
                var appRegistry={};
				for(var i in obj.tree){
                    var appStringArray=obj.tree[i].path.split("-");
                    var appName=appStringArray[0];
                    var versionString=appStringArray[1];
                    if(!versionString){
						continue;
					}
                    var versionArray=versionString.split(".");
                    var majorVersion=parseInt(versionArray[0]);
                    var minorVersion=parseInt(versionArray[1]);
                    var buildNumber=parseInt(versionArray[2]);
                    var versionArray=appRegistry.appName;
                    if(!versionArray){
                        versionArray=[];
                        appRegistry[appName]=versionArray;
                    }
                    versionArray.push({
                        majorVersion: majorVersion,
                        minorVersion: minorVersion,
                        buildNumber: buildNumber
                    });
                }
                var filepath=path.join(sceHome, "apps.json");
                fs.writeFileSync(filepath, JSON.stringify(appRegistry));
                console.log(appRegistry);
                console.log("updated, totally "+obj.tree.length+" apps");
			});

		});
	});
};
